package co.edu.unaula.gestionips.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

@Repository
@RequiredArgsConstructor

public class IpRepository {
    private final WebClient webClient;

    private static final String BASE_URL = "https://check.torproject.org/torbulkexitlist";
    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    public Mono<String> getIps() {
        return webClient
                .get()
                .uri(crearURI())
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse ->
                        clientResponse.bodyToMono(String.class));
    }

    private URI crearURI() {
        return UriComponentsBuilder
                .fromUriString(BASE_URL)
                .build()
                .toUri();
    }
}
