package co.edu.unaula.gestionips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionipsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionipsApplication.class, args);
	}

}
