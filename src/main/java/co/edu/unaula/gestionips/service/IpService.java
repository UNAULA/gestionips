package co.edu.unaula.gestionips.service;

import co.edu.unaula.gestionips.repository.IpRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

@Service
@Slf4j
public class IpService {
    private final IpRepository ipRepository;
    private final WebClient webClient;

    private static final String BASE_URL = "https://check.torproject.org/torbulkexitlist";
    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    @Autowired
    public IpService(IpRepository ipRepository, WebClient webClient) {
        this.ipRepository = ipRepository;
        this.webClient = webClient;
    }

    public Mono<String> obtenerIps() {
        log.info("Estoy en el Service.");
        return webClient
                .get()
                .uri(crearURI())
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse ->
                        clientResponse.bodyToMono(String.class));
    }

    private URI crearURI() {
        return UriComponentsBuilder
                .fromUriString(BASE_URL)
                .build()
                .toUri();
    }
}