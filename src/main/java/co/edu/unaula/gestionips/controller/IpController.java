package co.edu.unaula.gestionips.controller;

import co.edu.unaula.gestionips.service.IpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "api/")
public class IpController {
    private final IpService ipService;

    @Autowired
    public IpController(IpService ipService) {
        this.ipService = ipService;
    }

    @GetMapping(value = "ips")
    public Mono<String> obtenerIps() {
        return ipService.obtenerIps();
    }
}
